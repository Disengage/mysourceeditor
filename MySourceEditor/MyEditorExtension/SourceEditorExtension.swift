//
//  SourceEditorExtension.swift
//  MyEditorExtension
//
//  Created by Narongsak Kongpan on 10/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Foundation
import XcodeKit

class SourceEditorExtension: NSObject, XCSourceEditorExtension {
  func extensionDidFinishLaunching() {
    // If your extension needs to do any work at launch, implement this optional method.
  }
}
