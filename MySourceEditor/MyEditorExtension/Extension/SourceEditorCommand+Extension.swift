//
//  SourceEditorCommand+Extension.swift
//  MyEditorExtension
//
//  Created by Narongsak Kongpan on 13/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Foundation
import XcodeKit
import Cocoa

extension SourceEditorCommand {
  
  func makePublicInitializer(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) {
    let supportUTIs = [
      "com.apple.dt.playground", //
      "public.swift-source", //*.swift
      "com.apple.dt.playgroundpage" //
    ]
    
    // Allow content type
    let uti = invocation.buffer.contentUTI
    guard supportUTIs.contains(uti) else {
      completionHandler(nil)
      return
    }
    
    // Get ruby script absolute path
    guard let testFile = Bundle.main.url(forResource: "initializer", withExtension: "rb") else {
      completionHandler(nil)
      return
    }
    
    if let output = "which ruby".run() {
      let rubyPath = output.trimmingCharacters(in: .whitespacesAndNewlines)
      
      if let clipboard = "gem list".run() {
        let gem_list = clipboard.components(separatedBy: .whitespacesAndNewlines)
        
        let gem_installed = gem_list.filter { installed -> Bool in
          return installed.lowercased().contains("clipboard")
        }
        
        if gem_installed.contains("clipboard") {
          // Copy the selected text from xcode source editor to clipboard
          let editorContent: String = invocation.selectedLine().joined()
          let pasteboard = NSPasteboard.general
          pasteboard.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
          pasteboard.setString(editorContent, forType: NSPasteboard.PasteboardType.string)
          
          // Getting ruby script output from shell
          let targetPath = "\(rubyPath) \(testFile.path)"
          if let output = targetPath.run() {
            let content = output.trimmingCharacters(in: .whitespacesAndNewlines)
            
            // Extract output
            let responseContent = content.components(separatedBy: .newlines)
            
            // Set new source code into the current xcode source editor
            invocation.newEditorContent(from: responseContent)
            completionHandler(nil)
          }
        } else {
          let error = NSError.init(domain: "Required ruby clipboard, Try `gem install clipboard`.", code: -1, userInfo: nil)
          completionHandler(error)
        }
      } else {
        needInstallRuby(completion: completionHandler)
      }
    } else {
      needInstallRuby(completion: completionHandler)
    }
  }
  
  private func needInstallRuby(completion: @escaping (Error?) -> Void) {
    let error = NSError.init(domain: "Required ruby is not installed.", code: -1, userInfo: nil)
    completion(error)
  }
}
