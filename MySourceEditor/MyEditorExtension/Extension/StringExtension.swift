//
//  StringExtension.swift
//  SimpleTextEditor
//
//  Created by Sakarn Limnitikarn on 20/9/2561 BE.
//  Copyright © 2561 Sakarn Limnitikarn. All rights reserved.
//

import Foundation

extension String {
  func run() -> String? {
    let pipe = Pipe()
    let process = Process()
    process.launchPath = "/bin/sh"
    process.arguments = ["-c", self]
    process.standardOutput = pipe
    
    let fileHandle = pipe.fileHandleForReading
    process.launch()
    process.waitUntilExit()
    
    let data = fileHandle.readDataToEndOfFile()
    let result = String(data: data, encoding: .utf8)
    return result == "" ? nil : result
  }
}
