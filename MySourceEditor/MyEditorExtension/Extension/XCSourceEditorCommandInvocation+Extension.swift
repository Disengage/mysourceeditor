//
//  XCSourceEditorCommandInvocation+Extension.swift
//  MyEditorExtension
//
//  Created by Narongsak Kongpan on 13/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Foundation
import XcodeKit

extension XCSourceEditorCommandInvocation {
  func selectedLine() -> [String] {
    guard let selection = selectedRange() else {
      return []
    }
    var selectedText: [String] = []
    for (index, line) in self.buffer.lines.enumerated() {
      guard index >= selection.start.line && index <= selection.end.line else {
        continue
      }
      selectedText.append(line as! String)
    }
    return selectedText
  }
  
  func selectedText(removeTrailingNewLine: Bool? = true) -> String {
    // https://github.com/cyanzhong/xTextHandler
    guard let selection: XCSourceTextRange = self.selectedRange() else {
      return ""
    }
    
    let startLine = selection.start.line
    let startColumn = selection.start.column
    let endLine = selection.end.line
    let endColumn = selection.end.column
    
    var clipped: String
    if startLine == endLine && startColumn == endColumn { // select nothing
      clipped = self.buffer.lines[startLine] as! String
    }
    
    // enumerate lines
    for index in startLine...endLine {
      let line = self.buffer.lines[index] as! NSString
      
      if startLine == endLine { // single line
        clipped = line.substring(with: NSMakeRange(startColumn, endColumn - startColumn))
      } else if index == startLine { // first line
        clipped = line.substring(from: startColumn)
      } else if index == endLine { // last line
        clipped = line.substring(to: endColumn)
      } else { // common line
        clipped = line as String
      }
      
      if clipped.count > 0 {
        return removeTrailingNewLine ?? true ? clipped.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) : clipped
      }
    }
    return ""
  }
  
  func selectedRange() -> XCSourceTextRange? {
    let selectionText = self.buffer.selections
    guard selectionText.count > 0, let selection: XCSourceTextRange = selectionText.firstObject as? XCSourceTextRange else {
      return nil
    }
    return selection
  }
  
  func indentOfIndex(index: Int, lineCount: Int) -> String {
    let indentationWidth = self.buffer.indentationWidth
    guard indentationWidth > 0 else {
      return ""
    }
    if let selectRange = self.selectedRange() {
      let column = selectRange.start.column
      if index > 0 {
        let margin = String(repeating: " ", count: column)
        let padding = String(repeating: " ", count: indentationWidth)
        return index == lineCount - 1 ? margin : "\(margin)\(padding)"
      } else {
        return String(repeating: " ", count: column)
      }
    }
    return ""
  }
  
  func newEditorContent(from lineContent: [String]) {
    if let selectRange = self.selectedRange() {
      var newContent: [String] = []
      for index in 0 ..< selectRange.start.line {
        newContent.append(self.buffer.lines[index] as! String)
      }
      
      for (index, line) in lineContent.enumerated() {
        let indent = self.indentOfIndex(index: index, lineCount: lineContent.count)
        let content = line.trimmingCharacters(in: CharacterSet.whitespaces)
        newContent.append("\(indent)\(content)\n")
      }
      
      let next = self.selectedText().count == 0 ? 0 : 1
      for index in selectRange.end.line + next ..< self.buffer.lines.count {
        newContent.append(self.buffer.lines[index] as! String)
      }
      
      self.buffer.lines.removeAllObjects()
      for line in newContent {
        self.buffer.lines.add(line)
      }
    }
  }
}
