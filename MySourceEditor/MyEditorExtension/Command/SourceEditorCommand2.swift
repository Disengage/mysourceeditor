//
//  SourceEditorCommand2.swift
//  MyEditorExtension
//
//  Created by Narongsak Kongpan on 10/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Foundation
import XcodeKit

class SourceEditorCommand2: NSObject, XCSourceEditorCommand {
  
  func perform(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) -> Void {
    // Implement your command here, invoking the completion handler when done. Pass it nil on success, and an NSError on failure.
    
    if invocation.commandIdentifier == "Add-Unit-Test-Template" {
      SourceEditorCommand2.makeUnitTestTemplate(with: invocation, completionHandler: completionHandler)
    } else if invocation.commandIdentifier == "add-use-case-template" {
      SourceEditorCommand2.makeUseCaseTemplate(with: invocation, completionHandler: completionHandler)
    } else {
      completionHandler(nil)
    }
  }
}

extension SourceEditorCommand2 {
  static func makeUnitTestTemplate(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) {
    addContentFromTemplete(forResource: "unit_test", withExtension: "txt", with: invocation, completionHandler: completionHandler)
  }
  
  static func makeUseCaseTemplate(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) {
    addContentFromTemplete(forResource: "use_case", withExtension: "txt", with: invocation, completionHandler: completionHandler)
  }
  
  private static func addContentFromTemplete(forResource: String, withExtension: String, with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) {
    guard let templateUrl = Bundle.main.url(forResource: forResource, withExtension: withExtension) else {
      completionHandler(nil)
      return
    }
    do {
      // Read template content
      let content = try String(contentsOf: templateUrl, encoding: String.Encoding.utf8)
      let lines = content.components(separatedBy: .newlines)
      
      // Set new source code into the current xcode source editor
      invocation.newEditorContent(from: lines)
      completionHandler(nil)
    } catch (let error){
      completionHandler(error)
    }
  }
}
