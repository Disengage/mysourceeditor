//
//  SourceEditorCommand.swift
//  MyEditorExtension
//
//  Created by Narongsak Kongpan on 10/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Foundation
import XcodeKit

class SourceEditorCommand: NSObject, XCSourceEditorCommand {
  
  func perform(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) -> Void {
    // Implement your command here, invoking the completion handler when done. Pass it nil on success, and an NSError on failure.

    let productModuleName = Bundle.main.bundleIdentifier ?? ""
    if invocation.commandIdentifier == "\(productModuleName).MakePublicInitializer" {
      makePublicInitializer(with: invocation, completionHandler: completionHandler)
    } else if invocation.commandIdentifier == "\(productModuleName).CommandAction"{
      makeCustomEditorByCommand(with: invocation, completionHandler: completionHandler)
    }
  }

}

extension SourceEditorCommand {
  func makeCustomEditorByCommand(with invocation: XCSourceEditorCommandInvocation, completionHandler: @escaping (Error?) -> Void) {
    let commandIdentifier = invocation.selectedText()
    if commandIdentifier == "" {
      completionHandler(nil)
    } else {
      switch commandIdentifier {
      case "+test":
        SourceEditorCommand2.makeUnitTestTemplate(with: invocation, completionHandler: completionHandler)
      case "+usecase":
        SourceEditorCommand2.makeUseCaseTemplate(with: invocation, completionHandler: completionHandler)
      default:
        let error = NSError(domain: "No action supported for this command `\(commandIdentifier)`.", code: -1, userInfo: nil)
        completionHandler(error)
      }
    }
  }
}
