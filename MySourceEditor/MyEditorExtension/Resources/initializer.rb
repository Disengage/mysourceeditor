#!~/.rvm/rubies/ruby-2.5.1/bin/ ruby

#
# Pre-requisites:
# gem install clipboard
#
# To use this, copy some variable declarations to the clipboard, then execute this ruby script
#
# Returns: error code 1 if there are no declarations found.

require 'clipboard'

declarations = Clipboard.paste

parameters = declarations.scan(/(public )? ?(let|var) (\S+)\ ?: ?(\S+)/).collect { |ignore1, declaration_keyword, variable, type|
  ['public', declaration_keyword, variable, type]
}

parent_declaration = declarations.scan(/(public )?(\S+)\ (\w+)\ ?\n?(\{)\n(\s+|\S+)+(\})/).collect { |ignore1, type, keyword, ignore2, ignore3, ignore4|
  ['public', type, keyword]
}

exit 1 if parameters.empty?

declaration_line = parameters.collect { |accessibility, declaration_keyword, variable, type|
  "#{accessibility} #{declaration_keyword} #{variable}: #{type}"
}.join("\n")

parameter_line = parameters.collect { |accessibility, declaration_keyword, variable, type|
  "#{variable}: #{type}"
}.join(", ")

assignment_line = parameters.collect { |accessibility, declaration_keyword, variable, type|
  "self.#{variable} = #{variable}"
}.join("\n")

parent_line = parent_declaration.collect { |accessibility, type, declaration_keyword|
  "#{accessibility} #{type} #{declaration_keyword}"
}.join("")

public_declaration = <<END
@@PARENT_DECLARATION@@ {
  @@PUBLIC_INITIALIZER@@
}
END

public_initializer = <<END
@@DECLARATION@@

public init(@@PARAMETERS@@) {
  @@ASSIGNMENT@@
}
END

public_initializer.gsub!(/@@DECLARATION@@/, "#{declaration_line}")
public_initializer.gsub!(/@@PARAMETERS@@/, "#{parameter_line}")
public_initializer.gsub!(/@@ASSIGNMENT@@/, "#{assignment_line}")

if parent_declaration.count == 0
  puts public_initializer
else
  public_declaration.gsub!(/@@PUBLIC_INITIALIZER@@/, "#{public_initializer}")
  public_declaration.gsub!(/@@PARENT_DECLARATION@@/, "#{parent_line}")
  puts public_declaration
end
