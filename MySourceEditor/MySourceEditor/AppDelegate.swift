//
//  AppDelegate.swift
//  MySourceEditor
//
//  Created by Narongsak Kongpan on 14/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

extension String {
  func run() -> String? {
    let pipe = Pipe()
    let process = Process()
    process.launchPath = "/bin/sh"
    process.arguments = ["-c", self]
    process.standardOutput = pipe
    
    let fileHandle = pipe.fileHandleForReading
    process.launch()
    process.waitUntilExit()
    
    let data = fileHandle.readDataToEndOfFile()
    let result = String(data: data, encoding: .utf8)
    return result == "" ? nil : result
  }
}
