//
//  ViewController.swift
//  MySourceEditor
//
//  Created by Narongsak Kongpan on 14/1/2562 BE.
//  Copyright © 2562 Narongsak kongpan. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

  @IBOutlet weak var btnRuby: NSButton!
  @IBOutlet weak var btnClipboard: NSButton!
  
  @IBOutlet weak var lblRubyPath: NSTextField!
  @IBOutlet weak var lblClipboardPath: NSTextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    lblRubyPath.alphaValue  = 0.75
    lblClipboardPath.alphaValue  = 0.75
    
    lblRubyPath.textColor = NSColor.red
    lblClipboardPath.textColor = NSColor.red
    
    lblClipboardPath.stringValue = "Required Clipboard, Try `gem install clipboard`."
    lblRubyPath.stringValue = "Required Ruby, More info please visit `https://www.ruby-lang.org/en/`"
    
    btnRuby.state = .off
    btnClipboard.state = .off
  }
  
  override func viewDidAppear() {
    super.viewDidAppear()
    checkRequiredThings()
  }
  
  override func viewWillAppear() {
    super.viewWillAppear()
    checkRequiredThings()
  }

  private func checkRequiredThings() {
    if let output = "which ruby".run() {
      let rubyPath = output.trimmingCharacters(in: .whitespacesAndNewlines)
      btnRuby.state = .on
      lblRubyPath.stringValue = rubyPath
      lblRubyPath.textColor = NSColor.green
      
      if let clipboard = "gem list".run() {
        let gem_list = clipboard.components(separatedBy: .newlines)
        let gem_installed = gem_list.filter { installed -> Bool in
          return installed.lowercased().contains("clipboard")
        }
        if gem_installed.count > 0, let clipboard = gem_installed.first, clipboard.contains("clipboard") {
          btnClipboard.state = .on
          lblClipboardPath.stringValue = "\(clipboard) installed.".replacingOccurrences(of: "\n", with: " ")
          lblClipboardPath.textColor = NSColor.green
        }
      }
    }
  }
  
  @IBAction func onTapBtnCheckBox(_ sender: Any) {
    // Nothing
  }
  
  override var representedObject: Any? {
    didSet {
    // Update the view, if already loaded.
    }
  }
}
